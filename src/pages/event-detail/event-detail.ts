import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EventProvider } from '../../providers/event/event';
import { Camera } from '@ionic-native/camera';
import {HomePage} from '../home/home';
@IonicPage({
  name: 'event-detail',
  segment: 'event-detail/:eventId'
})
@Component({
  selector: 'page-event-detail',
  templateUrl: 'event-detail.html',
})
export class EventDetailPage {
  public currentEvent: any = {};
  public guestName:string = '';
  public guestPicture:string = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public eventProvider: EventProvider, public cameraPlugin: Camera) {}

  ionViewDidEnter(){
    this.eventProvider.getEventDetail(this.navParams.get('eventId'))
    .on('value', eventSnapshot => {
      this.currentEvent = eventSnapshot.val();
      this.currentEvent.id = eventSnapshot.key;
      
    });
  }
  removeItem(eventId:string){
    this.eventProvider.removePlant(eventId);
    this.navCtrl.setRoot(HomePage)
  
  }

  

}
