import { Component } from '@angular/core';
import { NavController,NavParams, Platform } from 'ionic-angular';
import { EventProvider } from '../../providers/event/event';
import { LocalNotifications } from '@ionic-native/local-notifications';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  notifications =[];
  notifID: number = 0;
  public eventList: Array<any>;
  constructor(public localNotifications: LocalNotifications,public navParams: NavParams,public navCtrl: NavController, public eventProvider: EventProvider, public platform: Platform) {}

  goToProfile(){ this.navCtrl.push('profile'); }

  goToCreate(){ this.navCtrl.push('eventcreate'); }

  ionViewDidEnter() {
  
    this.eventProvider.getEventList().on('value', snapshot => {
      this.eventList = [];
      snapshot.forEach( snap => {
        this.eventList.push({
          id: snap.key,
          name: snap.val().name,
          price: snap.val().price,
          date: snap.val().date,
          picture: snap.val().picture,
          notifyTime: snap.val().notifyTime
          
        });
        console.log("LOG FOR PLANTLIST", this.eventList);

        return false
      });
    });
    
  }
  ionViewDidLoad(){
      this.platform.ready().then(() => {
    console.log("-----------------ion view did load home page-------------------");
    this.localNotifications.hasPermission().then(function(granted) {
      
      if (!granted) {
        this.localNotifications.registerPermission();
      
      }
       if(this.platform.is('cordova')){
        let notifs = this.localNotifications.getScheduledIds();
        console.log("notifs log", notifs);
       } 
    });
    });
  }
  goToEventDetail(eventId){
    this.navCtrl.push('event-detail', { 'eventId': eventId });
  }

  removeItem(eventId:string){
    this.eventProvider.removePlant(eventId);
  }


}
