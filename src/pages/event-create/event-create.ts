import { Component } from '@angular/core';
import { IonicPage, NavController,Platform, AlertController } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';
import * as moment from 'moment';
import { EventProvider } from '../../providers/event/event';
import { Camera } from '@ionic-native/camera';
import {HomePage} from '../home/home';
@IonicPage({
  name: 'eventcreate'
})
@Component({
  selector: 'page-event-create',
  templateUrl: 'event-create.html',
})
export class EventCreatePage {
 //Camera property 
  guestPicture;
//LocalNotifications properties
//TODO-make notif deleteableS
  notifyTime: any;
  notifications: string;
  days: any[];
  chosenHours: number;
  chosenMinutes: number;
  plantNotification;
  firstNotificationTime;
  notifyString;
  constructor(public cameraPlugin: Camera, public navCtrl: NavController,
  public eventProvider: EventProvider, public platform: Platform,
  public alertCtrl: AlertController, public localNotifications: LocalNotifications) {

/************************************* 
Begin LocalNotification Constructor
*************************************/
    this.notifyTime = moment(new Date()).format();
 
        this.chosenHours = new Date().getHours();
        this.chosenMinutes = new Date().getMinutes();
 
        this.days = [
            {title: 'Monday', dayCode: 1, checked: false},
            {title: 'Tuesday', dayCode: 2, checked: false},
            {title: 'Wednesday', dayCode: 3, checked: false},
            {title: 'Thursday', dayCode: 4, checked: false},
            {title: 'Friday', dayCode: 5, checked: false},
            {title: 'Saturday', dayCode: 6, checked: false},
            {title: 'Sunday', dayCode: 0, checked: false}
        ];
/************************************* 
End LocalNotification Constructor
*************************************/          
  }

/************************************* 
Begin createEvent function
*************************************/
  createEvent(eventName: string,  guestPicture: string, notifyTime: any ) {
 
    this.notifyString = this.firstNotificationTime.toString();
       this.eventProvider.createEvent(eventName, this.guestPicture, this.notifyString);
    
      this.navCtrl.push(HomePage);
  
  }
/************************************* 
End createEvent function
*************************************/

/************************************* 
Begin takePicture function
*************************************/
  takePicture(){
    this.cameraPlugin.getPicture({
      quality : 95,
      destinationType : this.cameraPlugin.DestinationType.DATA_URL,
      sourceType : this.cameraPlugin.PictureSourceType.CAMERA,
      allowEdit : true,
      encodingType: this.cameraPlugin.EncodingType.PNG,
      targetWidth: 500,
      targetHeight: 500,
      saveToPhotoAlbum: true
    }).then(imageData => {
      this.guestPicture = imageData;
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }
/************************************* 
End takePicture function
*************************************/  

  ionViewDidLoad(){
    this.platform.ready().then(() => {
    console.log("-----------------ion view did load event create page-------------------");
    this.localNotifications.hasPermission().then(function(granted) {
      
      if (!granted) {
        this.localNotifications.registerPermission();
      } 
    });
    });
  }

/************************************* 
Begin LocalNotification and moment functions
*************************************/  
  timeChange(time){
    this.chosenHours = time.hour;
    this.chosenMinutes = time.minute;
  }

  addNotifications(eventName: string){
    let currentDate = new Date();
    let currentDay = currentDate.getDay(); // Sunday = 0, Monday = 1, etc.
  
    for(let day of this.days){
      if(day.checked){
        this.firstNotificationTime = new Date();
        let dayDifference = day.dayCode - currentDay;

        if(dayDifference < 0){
            dayDifference = dayDifference + 7; // for cases where the day is in the following week
        }

        this.firstNotificationTime.setHours(this.firstNotificationTime.getHours() + (24 * (dayDifference)));
        this.firstNotificationTime.setHours(this.chosenHours);
        this.firstNotificationTime.setMinutes(this.chosenMinutes);

         this.plantNotification = {
            id: day.dayCode,
            title: eventName,
            text: 'Reminder to water! :)',
            at: this.firstNotificationTime,
            every: 'week',
        };

        this.notifications = this.plantNotification;

      }
  
    }
  
      console.log("Notifications to be scheduled: ", this.notifications);
  
      if(this.platform.is('cordova')){
        
              // Schedule the new notifications
              this.localNotifications.schedule(this.plantNotification);
             
  
              let alert = this.alertCtrl.create({
                  title: 'Notifications set',
                  buttons: ['Ok']
              });
  
              alert.present();
  
          
  
      }
  
  }

  cancelAll(){
    this.localNotifications.cancelAll();
  
      let alert = this.alertCtrl.create({
          title: 'Notifications cancelled',
          buttons: ['Ok']
      });
  
      alert.present();
  }
/************************************* 
End LocalNotification and Moment functions
*************************************/      

}